﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using MarcReader;

namespace Example
{
    public class Program
    {
        private static readonly string RAW_PATH = @"C:\Users\Dmitry\Downloads\5_ek_rgbi\5_ek_rgbi.iso";
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string rawMarc = File.ReadAllText(RAW_PATH);
            FileMARC marcRecords = new FileMARC(rawMarc);
            var total = marcRecords.Count;
            Console.WriteLine($"Всего {total} записей");
            int withoutAuthor = 0;
            int current = 0;
            foreach (Record record in marcRecords)
            {
                //находим запись с тегом 200
                Field authorField = record["200"];
                if(authorField == null)
                {
                    withoutAuthor++;
                    continue;
                }
                //проверяем что это полу не является контрольным (можно и не проверять, оно вряд ли будет таковым)
                if (authorField.IsDataField())
                {
                    DataField authorDataField = (DataField)authorField;
                    //выбираем имя автора (фамилия+инициалы)
                    string authorName = authorDataField['f'].Data;
                }
                //таким образом можно получить все подполя с тегом 200
                //List<Field> subjects = record.GetFields("200");

                current++;

                Console.Write($"\r{current}\\{total}");
            }
            Console.WriteLine($"Всего {withoutAuthor} записей не имеет авторов");
        }
    }
}
